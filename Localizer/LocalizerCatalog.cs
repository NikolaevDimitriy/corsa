using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NGettext;

namespace Corsa.Localizer {
  /// <summary>
  /// Описание каталога перевода и его домена
  /// </summary>
  public struct LocalizerCatalog {
    public Catalog Catalog { get; }
    public string Domain { get; }

    public CultureInfo Culture => Catalog.CultureInfo;

    public LocalizerCatalog(Catalog catalog, string domain) {
      Catalog = (Catalog)catalog;
      Domain = domain;
    }
  }
}
