﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Corsa.Localizer {
  public class LocalizerImproperlyConfigured : Exception {
    public LocalizerImproperlyConfigured() : base() { }
    public LocalizerImproperlyConfigured(string message) : base(message) { }
    public LocalizerImproperlyConfigured(string message, Exception innerException) : base(message, innerException) { }
  }
}
