using NGettext;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;

namespace Corsa.Localizer {
  public class Localizer : ILocalizer {

    private readonly IDictionary<string, Catalog> _catalogs;

    private readonly ConcurrentDictionary<Type, Lazy<EnumLocalizer>> _enumCache = new ConcurrentDictionary<Type, Lazy<EnumLocalizer>>();
    private readonly ConcurrentDictionary<Type, Lazy<Delegate>> _funcCache = new ConcurrentDictionary<Type, Lazy<Delegate>>();

    /// <summary>
    /// Домен перевода по умолчанию
    /// </summary>
    public string DefaultDomain { get; }

    public Localizer(IEnumerable<LocalizerCatalog> catalogs, string defaultDomain = null) {
      if (catalogs == null || !catalogs.Any())
        throw new LocalizerImproperlyConfigured();
      _catalogs = catalogs.ToDictionary(c => GetKey(c.Culture.Name, c.Domain), el => el.Catalog);
      DefaultDomain = defaultDomain;
    }

    public string _(string text, string domain = null) {
      return GetCatalog(domain)?.GetString(text) ?? text;
    }

    public string _(string text, params object[] args) {
      return GetCatalog(DefaultDomain)?.GetString(text, args) ?? text;
    }

    public string _(string text, string domain = null, params object[] args) {
      return GetCatalog(domain)?.GetString(text, args) ?? text;
    }

    public string _error(string baseText, int error_code, object[] args = null, string domain = null) {
      var key = $"Error.{error_code}";
      if (domain != null) {
        return GetCatalog(domain)?.GetString(key, args) ?? GetCatalog(domain)?.GetString(baseText, args) ?? baseText;
      }
      foreach (var c in GetCatalogsForCulture(CultureInfo.CurrentUICulture)) {
        var text = c.GetStringDefault(key, null) ?? c.GetStringDefault(baseText, null);
        if (text != null)
          return args == null
              ? text
              : string.Format(text, args);
      }
      return args == null
          ? baseText
          : string.Format(baseText, args);
    }

    private string GetEnum(Type tEnum, int value, string domain = null) {
      if (!tEnum.IsEnum || !tEnum.IsEnumDefined(value))
        return string.Empty;
      var item = _enumCache.GetOrAdd(tEnum, _ =>
                                              new Lazy<EnumLocalizer>(() => new EnumLocalizer(tEnum)
                                                                      , LazyThreadSafetyMode.ExecutionAndPublication)
                                    ).Value;
      return item.GetString(value, (Catalog)GetCatalog(domain ?? item.Domain));
    }

    public string _e<TEnum>(int value, string domain = null) where TEnum : struct {
      return GetEnum(typeof(TEnum), value, domain);
    }

    public string _e<TEnum>(TEnum value, string domain = null) where TEnum : struct {
      Type tEnum = typeof(TEnum);
      var asInt = (Func<TEnum, int>)_funcCache.GetOrAdd(tEnum, _ => new Lazy<Delegate>(() => {
        var paramT = Expression.Parameter(tEnum);
        UnaryExpression bodyToInt = Expression.Convert(paramT, typeof(int));
        return Expression.Lambda<Func<TEnum, int>>(bodyToInt, paramT).Compile();
      }, LazyThreadSafetyMode.ExecutionAndPublication)
      ).Value;
      return GetEnum(tEnum, asInt(value), domain);
    }

    public string _e(Type tEnum, int value, string domain = null) {
      return GetEnum(tEnum, value, domain);
    }


    public IEnumerable<string> _es<TEnum>(IEnumerable<TEnum> values, string domain = null) where TEnum : struct {
      foreach (var value in values)
        yield return _e(value, domain);
    }
    public IEnumerable<string> _es<TEnum>(IEnumerable<int> values, string domain = null) where TEnum : struct {
      foreach (var value in values)
        yield return _e(value, domain);
    }
    public IEnumerable<string> _es(Type tEnum, IEnumerable<int> values, string domain = null) {
      foreach (var value in values)
        yield return _e(tEnum, value, domain);
    }

    public string _n(string text, string pluralText, long n, string domain = null) {
      return GetCatalog(domain)?.GetPluralString(text, pluralText, n) ?? text;
    }

    public string _n(string text, string pluralText, long n, params object[] args) {
      return GetCatalog(DefaultDomain)?.GetPluralString(text, pluralText, n, args) ?? text;
    }


    public string _n(string text, string pluralText, long n, string domain = null, params object[] args) {
      return GetCatalog(domain)?.GetPluralString(text, pluralText, n, args) ?? text;
    }

    public string _p(string context, string text, string domain = null) {
      return GetCatalog(domain)?.GetParticularString(context, text) ?? text;
    }

    public string _p(string context, string text, params object[] args) {
      return GetCatalog(DefaultDomain)?.GetParticularString(context, text, args) ?? text;
    }

    public string _p(string context, string text, string domain = null, params object[] args) {
      return GetCatalog(domain)?.GetParticularString(context, text, args) ?? text;
    }

    public string _pn(string context, string text, string pluralText, long n, string domain = null) {
      return GetCatalog(domain)?.GetParticularPluralString(context, text, pluralText, n) ?? text;
    }

    public string _pn(string context, string text, string pluralText, long n, params object[] args) {
      return GetCatalog(DefaultDomain)?.GetParticularPluralString(context, text, pluralText, n, args) ?? text;
    }

    public string _pn(string context, string text, string pluralText, long n, string domain = null, params object[] args) {
      return GetCatalog(domain)?.GetParticularPluralString(context, text, pluralText, n, args) ?? text;
    }

    public IDictionary<string, string[]> GetTranslations(string domain = null) {
      return ((Catalog)GetCatalog(domain)).Translations;
    }

    internal ICatalog GetCatalog(string domain) {
      domain = !string.IsNullOrWhiteSpace(domain) ? domain : DefaultDomain;
      string key = GetKey(CultureInfo.CurrentUICulture.Name, domain);
      if (_catalogs.ContainsKey(key))
        return _catalogs[key];
      key = GetKey(CultureInfo.CurrentUICulture.TwoLetterISOLanguageName, domain);
      if (_catalogs.ContainsKey(key))
        return _catalogs[key];
      return null;
    }

    private IEnumerable<Catalog> GetCatalogsForCulture(CultureInfo culture) => _catalogs.Values.Where(c => c.CultureInfo == culture);

    /// <summary>
    /// Получить ключ для комбинации культуры и домена
    /// </summary>
    /// <remarks>
    /// Ключ формируется в виде domain.culture_name. Например: Frontend.ru-RU
    /// </remarks>
    private static string GetKey(string culture, string domain) => string.Concat(domain, ".", culture);
  }
}
