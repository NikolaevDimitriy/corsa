using NGettext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Corsa.Localizer {
  internal class EnumLocalizer {
    private struct EnumItem {
      public string FullName { get; }

      public string Description { get; }

      public EnumItem(string fullName, string description) {
        FullName = fullName;
        Description = description;
      }
    }

    public Type EnumType { get; }

    public string Domain => EnumType.Assembly.GetName().Name;

    private readonly IDictionary<int, EnumItem> _descriptionMap = new Dictionary<int, EnumItem>();

    public EnumLocalizer(Type @enum) {
      EnumType = @enum;
      _descriptionMap = EnumType.GetFields(BindingFlags.Public | BindingFlags.Static)
          .Select(f => new {
            Description = f.GetCustomAttributes(typeof(DescriptionAttribute), false)
                                  .Cast<DescriptionAttribute>()
                                  .FirstOrDefault()?.Description,
            Key = (int)f.GetRawConstantValue()
          })
          .Select(i => new { Key = i.Key, Description = i.Description, FullName = GetFullName(i.Key) })
          .ToDictionary(i => i.Key, i => new EnumItem(i.FullName, i.Description ?? i.FullName));
    }

    public string GetString(int value, Catalog catalog) {
      if (_descriptionMap.TryGetValue(value, out EnumItem item)) {
        return catalog?.GetStringDefault(item.FullName, item.Description) ?? item.Description;
      }
      return GetFullName(value);
    }

    private string GetFullName(int value) => string.Format("{0}.{1}",
                                                           EnumType.FullName.Replace('+', '.'),
                                                           Enum.GetName(EnumType, value));
  }
}
