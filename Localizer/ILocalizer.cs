﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Corsa.Localizer {
  /// <summary>
  /// Общий интерфейс получения локализованных текстовых данных
  /// </summary>
  public interface ILocalizer {
    /// <summary>
    /// Получить локализованный текст
    /// </summary>
    /// <param name="text">Текст</param>
    /// <param name="domain">Контекст приложения</param>
    string _(string text, string domain = null);

    /// <summary>
    /// Получить локализованный текст с форматом (string.Format)
    /// </summary>
    /// <param name="text">Текст</param>
    /// <param name="args">Аргументы формата</param>
    string _(string text, params object[] args);

    /// <summary>
    /// Получить локализованный текст с форматом (string.Format)
    /// </summary>
    /// <param name="text">Текст</param>
    /// <param name="domain">Контекст приложения</param>
    /// <param name="args">Аргументы формата</param>
    string _(string text, string domain = null, params object[] args);

    /// <summary>
    /// Получить локализованный текст ошибки
    /// </summary>
    /// <param name="baseText">Базовый текст ошибки</param>
    /// <param name="error_code">Код ошибки</param>
    /// <param name="args">Вспомогательные аргументы формата</param>
    /// <param name="domain">Контекст приложения</param>
    string _error(string baseText, int error_code, object[] args = null, string domain = null);

    /// <summary>
    /// Получить локализованное описание элемента перечисления
    /// </summary>
    /// <param name="value">Целочисленное значение перечисления</param>
    /// <param name="domain">Контекст приложения (если отличается от конвенции)</param>
    string _e<TEnum>(int value, string domain = null) where TEnum : struct;

    /// <summary>
    /// Получить локализованное описание элемента перечисления
    /// </summary>
    /// <param name="value">Значение перечисления</param>
    /// <param name="domain">Контекст приложения (если отличается от конвенции)</param>
    string _e<TEnum>(TEnum value, string domain = null) where TEnum : struct;

    /// <summary>
    /// Получить локализованное описание элемента перечисления
    /// </summary>
    /// <param name="tEnum">Тип перечисления</param>
    /// <param name="value">Значение перечисления</param>
    /// <param name="domain">Контекст приложения (если отличается от конвенции)</param>
    string _e(Type tEnum, int value, string domain = null);

    /// <summary>
    /// Получить локализованные описания набора элементов перечисления
    /// </summary>
    /// <param name="values">Значения перечисления</param>
    /// <param name="domain">Контекст приложения (если отличается от конвенции)</param>
    IEnumerable<string> _es<TEnum>(IEnumerable<TEnum> values, string domain = null) where TEnum : struct;

    /// <summary>
    /// Получить локализованные описания набора элементов перечисления
    /// </summary>
    /// <param name="values">Значения перечисления</param>
    /// <param name="domain">Контекст приложения (если отличается от конвенции)</param>
    IEnumerable<string> _es<TEnum>(IEnumerable<int> values, string domain = null) where TEnum : struct;

    /// <summary>
    /// Получить локализованные описания набора элементов перечисления
    /// </summary>
    /// <param name="tEnum">Тип перечисления</param>
    /// <param name="values">Значения перечисления</param>
    /// <param name="domain">Контекст приложения (если отличается от конвенции)</param>
    IEnumerable<string> _es(Type tEnum, IEnumerable<int> values, string domain = null);

    /// <summary>
    /// Получить локализованную форму множественного числа
    /// </summary>
    /// <param name="text">Текст в единственном числе</param>
    /// <param name="pluralText">Текст во множественном числе</param>
    /// <param name="n">Количество</param>
    /// <param name="domain">Контекст приложения</param>
    string _n(string text, string pluralText, long n, string domain = null);

    /// <summary>
    /// Получить локализованную форму множественного числа с форматом (string.Format)
    /// </summary>
    /// <param name="text">Текст в единственном числе</param>
    /// <param name="pluralText">Текст во множественном числе</param>
    /// <param name="n">Количество</param>
    /// <param name="args">Аргументы формата</param>
    string _n(string text, string pluralText, long n, params object[] args);

    /// <summary>
    /// Получить локализованную форму множественного числа с форматом (string.Format)
    /// </summary>
    /// <param name="text">Текст в единственном числе</param>
    /// <param name="pluralText">Текст во множественном числе</param>
    /// <param name="n">Количество</param>
    /// <param name="domain">Контекст приложения</param>
    /// <param name="args">Аргументы формата</param>
    string _n(string text, string pluralText, long n, string domain = null, params object[] args);

    /// <summary>
    /// Получить локализованный текст по контексту сообщения
    /// </summary>
    /// <param name="context">Контекст сообщения</param>
    /// <param name="text">Текст</param>
    /// <param name="domain">Контекст приложения</param>
    string _p(string context, string text, string domain = null);

    /// <summary>
    /// Получить локализованный текст по контексту сообщения с форматом (string.Format)
    /// </summary>
    /// <param name="context">Контекст сообщения</param>
    /// <param name="text">Текст</param>
    /// <param name="args">Аргументы формата</param>
    string _p(string context, string text, params object[] args);

    /// <summary>
    /// Получить локализованный текст по контексту сообщения с форматом (string.Format)
    /// </summary>
    /// <param name="context">Контекст сообщения</param>
    /// <param name="text">Текст</param>
    /// <param name="domain">Контекст приложения</param>
    /// <param name="args">Аргументы формата</param>
    string _p(string context, string text, string domain = null, params object[] args);

    /// <summary>
    /// Получить локализованный текст во множественном числе по контексту сообщения
    /// </summary>
    /// <param name="context">Контекст сообщения</param>
    /// <param name="text">Текст</param>
    /// <param name="pluralText">Текст во множественном числе</param>
    /// <param name="n">Количество</param>
    /// <param name="domain">Контекст приложения</param>
    string _pn(string context, string text, string pluralText, long n, string domain = null);

    /// <summary>
    /// Получить локализованный текст во множественном числе по контексту сообщения с форматом (string.Format)
    /// </summary>
    /// <param name="context">Контекст сообщения</param>
    /// <param name="text">Текст</param>
    /// <param name="pluralText">Текст во множественном числе</param>
    /// <param name="n">Количество</param>
    /// <param name="args">Аргументы формата</param>
    string _pn(string context, string text, string pluralText, long n, params object[] args);

    /// <summary>
    /// Получить локализованный текст во множественном числе по контексту сообщения с форматом (string.Format)
    /// </summary>
    /// <param name="context">Контекст сообщения</param>
    /// <param name="text">Текст</param>
    /// <param name="pluralText">Текст во множественном числе</param>
    /// <param name="n">Количество</param>
    /// <param name="domain">Контекст приложения</param>
    /// <param name="args">Аргументы формата</param>
    string _pn(string context, string text, string pluralText, long n, string domain = null, params object[] args);

    /// <summary>
    /// Получить все переводы
    /// </summary>
    /// <param name="domain">Контекст приложения</param>
    IDictionary<string, string[]> GetTranslations(string domain = null);
  }
}
