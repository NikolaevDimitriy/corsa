﻿using System;

namespace Corsa.Interfaces.Models {
  public interface IDeletableModel<T> : IModel<T> {
    bool IsDeleted { get; set; }
    DateTime? DeletionTimestamp { get; set; }
  }
}
