using System.Globalization;

namespace Corsa.Interfaces.Models {
  public interface IRequester<TId> : IModel<TId> {
    string Email { get; }
    bool IsSuperAdmin { get; }
    bool IsActive { get; }
    CultureInfo Culture { get; set; }
  }
}
