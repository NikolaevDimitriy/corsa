namespace Corsa.Interfaces.Models {
  /// <summary>
  /// Единая структура хранения перечисления в виде массива бит
  /// </summary>
  /// <typeparam name="T">Тип перечисления</typeparam>
  public interface IEnumCompressed<T> : ISequenceComparable<IEnumCompressed<T>> where T : struct {

    /// <summary>
    /// Признак, что структура пуста
    /// </summary>
    bool IsEmpty { get; }

    /// <summary>
    /// Получить копию внутреннего массива данных
    /// </summary>
    int[] ToArray();

    /// <summary>
    /// Получить установленные значения
    /// </summary>
    T[] GetValues();

    /// <summary>
    /// Установить флаг (значение)
    /// </summary>
    void Set(T item);

    /// <summary>
    /// Инвертировать текущий набор
    /// </summary>
    void Invert();

    /// <summary>
    /// Установка нескольких флагов (значений) за раз
    /// </summary>
    /// <param name="items">Массив значений</param>
    void Set(params T[] items);

    /// <summary>
    /// Снять флаг (значение)
    /// </summary>
    void Unset(T item);

    /// <summary>
    /// Снять несколько флагов (значений) за раз
    /// </summary>
    /// <param name="items">Массив значений</param>
    void Unset(params T[] items);

    /// <summary>
    /// Проверить, что все переданные значения установлены
    /// </summary>
    /// <param name="items">Массив значений</param>
    bool All(params T[] items);

    /// <summary>
    /// Проверить, что значение установлено
    /// </summary>
    bool Contains(T item);

    /// <summary>
    /// Проверка уставноки хотя бы одного значения
    /// </summary>
    /// <param name="items">Массив значений для проверки</param>
    /// <returns>Истина, если хотя бы одно значение установлено</returns>
    bool Any(params T[] items);

    /// <summary>
    /// Объединение наборов значений перечисления
    /// </summary>
    /// <param name="otherSet">Второй набор значений перечисления</param>
    /// <returns>Новый объединённый набор значений перечисления</returns>
    IEnumCompressed<T> Union(IEnumCompressed<T> otherSet);

    /// <summary>
    /// Пересечение наборов значений перечисления
    /// </summary>
    /// <param name="otherSet">Второй набор значений перечисления</param>
    /// <returns>Новый набор общих значений перечисления</returns>
    IEnumCompressed<T> Intersect(IEnumCompressed<T> otherSet);
  }
}
