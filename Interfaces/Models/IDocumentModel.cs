﻿using System;

namespace Corsa.Interfaces.Models {
  public interface IDocumentModel<T> : IDeletableModel<T> {
    IIdValue<int, string> Creator { get; set; }
    DateTime CreationTimestamp { get; set; }
    DateTime LastUpdateTimestamp { get; set; }
  }
}