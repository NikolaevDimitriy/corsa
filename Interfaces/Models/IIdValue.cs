﻿namespace Corsa.Interfaces.Models {
  public interface IIdValue<TId, TVal> : IModel<TId> {
    TVal Value { get; }
  }
}
