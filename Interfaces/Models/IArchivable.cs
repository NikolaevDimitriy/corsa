﻿namespace Corsa.Interfaces.Models {
  public interface IArchivable {
    bool IsArchived { get; set; }
  }
}
