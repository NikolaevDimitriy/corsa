﻿namespace Corsa.Interfaces.Models {
  public interface IModel<T> {
    T Id { get; set; }
    bool IsNew { get; }
  }
}
