using System.Collections.Generic;

namespace Corsa.Interfaces.Models {
  public interface IPagingList<out T> : IEnumerable<T> {
    long Total { get; }
  }
}