﻿using System.Collections.Generic;

namespace Corsa.Interfaces.Data {
  public interface IModelBuilder<TSource, TTarget> : IBuilder<TSource, TTarget> {
    void IndexFields(IDictionary<string, int> columnOrdinalsDictionary);
  }
}