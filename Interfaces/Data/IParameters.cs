using System;
using System.Collections.Generic;

namespace Corsa.Interfaces.Data {
  public interface IParameters<T> {
    IEnumerable<T> GetParameters(Func<T> createParameter);
  }
}
