using System.Threading.Tasks;
using Corsa.Interfaces.Models;

namespace Corsa.Interfaces.Data {
  public interface IModelPaginatorStorage<TItem, in TSourceParam, TTargetParam>
    where TSourceParam : IPagingParameters<TTargetParam> {
    Task<long> GetTotalCount(TSourceParam parameters);
    Task<IPagingList<TItem>> GetPage(TSourceParam parameters);
  }
}