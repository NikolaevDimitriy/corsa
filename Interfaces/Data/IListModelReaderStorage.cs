﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Corsa.Interfaces.Data {
  public interface IListModelReaderStorage<TItem, in TSourceParam, TTargetParam> 
    where TSourceParam : IParameters<TTargetParam> {
    Task<IEnumerable<TItem>> GetList(TSourceParam parameters);
  }
}