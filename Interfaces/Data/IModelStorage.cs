﻿using System.Threading.Tasks;

namespace Corsa.Interfaces.Data {
  public interface IModelStorage<TItem, in TSourceParam, TTargetParam> :
      IModelReaderStorage<TItem, TSourceParam, TTargetParam>,
      IModelWriterStorage<TItem, TSourceParam, TTargetParam>
      where TSourceParam : IParameters<TTargetParam> {
    Task<bool> Delete(TItem item, TSourceParam p);
  }
}
