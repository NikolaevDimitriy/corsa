﻿namespace Corsa.Interfaces.Data {
  public interface IBuilder<TSource, TTarget> {
    TTarget Build(TSource obj);
  }
}