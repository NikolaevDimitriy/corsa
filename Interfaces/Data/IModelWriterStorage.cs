﻿using System.Threading.Tasks;

namespace Corsa.Interfaces.Data {
  public interface IModelWriterStorage<TItem, in TSourceParam, TTargetParam>
    where TSourceParam : IParameters<TTargetParam> {
    Task<TItem> Save(TItem item, TSourceParam p);
  }
}