﻿using System.Threading.Tasks;
using System.Collections.Generic;
namespace Corsa.Interfaces.Data {
  public interface IListModelStorage<TItem, in TSourceParam, TTargetParam> :
      IListModelReaderStorage<TItem, TSourceParam, TTargetParam>,
      IListModelWriterStorage<TItem, TSourceParam, TTargetParam>
      where TSourceParam : IParameters<TTargetParam> {
    Task<bool> DeleteList(IEnumerable<TItem> items, TSourceParam parameters);
  }
}
