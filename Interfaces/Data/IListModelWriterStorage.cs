﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Corsa.Interfaces.Data {
  public interface IListModelWriterStorage<TItem, in TSourceParam, TTargetParam>
    where TSourceParam : IParameters<TTargetParam> {
    Task<IEnumerable<TItem>> SaveList(IEnumerable<TItem> items, TSourceParam parameters);
  }
}