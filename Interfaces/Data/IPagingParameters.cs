using System;
using System.Collections.Generic;

namespace Corsa.Interfaces.Data {
  public interface IPagingParameters<T> : IParameters<T> {
    IEnumerable<T> GetFilterParameters(Func<T> createParameter);
  }
}