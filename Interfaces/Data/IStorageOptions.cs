﻿namespace Corsa.Interfaces.Data {
  public interface IStorageOptions {
    string MasterConnectionString { get; }
    string SlaveConnectionString { get; }
  }
}
