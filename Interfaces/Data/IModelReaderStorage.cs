﻿using System.Threading.Tasks;

namespace Corsa.Interfaces.Data {
  public interface IModelReaderStorage<TItem, in TSourceParam, TTargetParam>
    where TSourceParam : IParameters<TTargetParam> {
    Task<TItem> Get(TSourceParam param);
  }
}