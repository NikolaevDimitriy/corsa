﻿using System;

namespace Corsa.Utils {
  public static class StringHelper {
    /// <summary>
    /// Returns the input string with the first character converted to uppercase
    /// </summary>
    public static string FirstLetterToUpperCase(this string source) {
      if (string.IsNullOrEmpty(source)) {
        throw new ArgumentException("There is no first letter");
      }
      char[] chars = source.ToCharArray();
      chars[0] = char.ToUpper(chars[0]);

      return new string(chars);
    }
  }
}
