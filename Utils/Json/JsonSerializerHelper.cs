using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Corsa.Utils.Json {
  public static class JsonSerializerHelper {
    public static bool IsCamelCaseNaming(this JsonSerializer serializer) {
      var isCamelCaseNaming = false;
      if (serializer.ContractResolver is DefaultContractResolver resolver)
        isCamelCaseNaming = resolver.NamingStrategy is CamelCaseNamingStrategy;

      return isCamelCaseNaming;
    }
  }
}