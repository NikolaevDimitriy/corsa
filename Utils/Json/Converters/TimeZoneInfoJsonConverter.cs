using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Corsa.Utils;

namespace Corsa.Utils.Json.Converters {

  public class TimeZoneInfoJsonConverter : JsonConverter<TimeZoneInfo> {

    public override TimeZoneInfo ReadJson(JsonReader reader, Type objectType, TimeZoneInfo existingValue, bool hasExistingValue, JsonSerializer serializer) {
      var token = JToken.Load(reader);

      switch (token.Type) {
        case JTokenType.String:
          return token.Root.ToObject<string>().GetTimeZone();
        case JTokenType.Object:
          return token.SelectToken("Id")?.ToObject<string>().GetTimeZone() ?? token.SelectToken("id")?.ToObject<string>().GetTimeZone();
        default:
          return null;
      }
    }

    public override void WriteJson(JsonWriter writer, TimeZoneInfo value, JsonSerializer serializer) {
      var token = new JValue(value.Id);
      token.WriteTo(writer);
    }
  }
}