﻿using System.Globalization;

namespace Corsa.Utils {
  public static class CultureHelper {
    private const string RUSSIAN = "ru-RU";

    public static CultureInfo GetCulture(this string culture) {
      try {
        if (string.IsNullOrEmpty(culture))
          return new CultureInfo(RUSSIAN);
        return new CultureInfo(culture);
      } catch {
        return new CultureInfo(RUSSIAN);
      }
    }
  }
}
