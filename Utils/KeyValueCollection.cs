using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Corsa.Utils {
  /// <summary>
  /// Generic key-value collection (readonly)
  /// </summary>
  public class KeyValueCollection<TKey, TValue> : IReadOnlyCollection<KeyValuePair<TKey, TValue>> {
    private KeyValuePair<TKey, TValue>[] _array;
    private IDictionary<TKey, int> _index;

    public KeyValueCollection(IEnumerable<KeyValuePair<TKey, TValue>> collection) {
      _array = collection.ToArray();
      _index = _array.Select((kv, i) => new { Key = kv.Key, Index = i }).ToDictionary(a => a.Key, a => a.Index);
    }

    public KeyValueCollection(IDictionary<TKey, TValue> dictionary) {
      _array = dictionary.ToArray();
      _index = _array.Select((kv, i) => new { Key = kv.Key, Index = i }).ToDictionary(a => a.Key, a => a.Index);
    }

    public int Count => _array?.Length ?? 0;

    public TValue this[TKey key] => _index.ContainsKey(key) ? _array[_index[key]].Value : default(TValue);

    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
      return _array.Select(kv => kv).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
      return _array.GetEnumerator();
    }

    public bool ContainsKey(TKey key) {
      return _index.ContainsKey(key);
    }

    public bool TryGetValue(TKey key, out TValue value) {
      if (ContainsKey(key)) {
        value = this[key];
        return true;
      }
      value = default(TValue);
      return false;
    }
  }

  public static class KeyValueCollectionExtentions {
    public static KeyValueCollection<TKey, TValue> ToKeyValueCollection<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> collection) {
      return new KeyValueCollection<TKey, TValue>(collection);
    }

    public static KeyValueCollection<TKey, TValue> ToKeyValueCollection<TKey, TValue>(this IDictionary<TKey, TValue> dictionary) {
      return new KeyValueCollection<TKey, TValue>(dictionary);
    }
  }
}