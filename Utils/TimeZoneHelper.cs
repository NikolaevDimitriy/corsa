using System;
using System.Runtime.InteropServices;
using TimeZoneConverter;

namespace Corsa.Utils {
  public static class TimeZoneHelper {
    private static readonly string RussianTimeZone = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
        ? "Russian Standard Time" : "Europe/Moscow";
    private static TimeZoneInfo _moscowTimeZone = GetTimeZone(RussianTimeZone);

    public static TimeZoneInfo GetTimeZone(this string id) {
      try {
        if (string.IsNullOrEmpty(id))
          id = RussianTimeZone;

        return TimeZoneInfo.FindSystemTimeZoneById(id);
      } catch {
        try {
          var tzid = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
              ? TZConvert.IanaToWindows(id)
              : TZConvert.WindowsToIana(id);
          return TimeZoneInfo.FindSystemTimeZoneById(tzid);
        } catch {
          return MoscowTimeZone;
        }
      }
    }
    public static TimeZoneInfo GetWinTimeZone(this TimeZoneInfo tz) {
      bool isWin = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
      TimeZoneInfo timeZoneInfo = tz;
      if (!isWin) {
        try {
          timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(TZConvert.IanaToWindows(tz.Id));
        } catch {
          timeZoneInfo = null;
        }
      }
      return timeZoneInfo;
    }
    public static TimeZoneInfo Check(this TimeZoneInfo val) {
      return val ?? _moscowTimeZone;
    }

    public static TimeZoneInfo MoscowTimeZone {
      get {
        return _moscowTimeZone;
      }
    }


  }
}
