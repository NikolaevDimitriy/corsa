using System;

namespace Corsa.Utils {
  public static class DateTimeExtensions {
    public static DateTime SpecifyAsUtc(this DateTime value) {
      return DateTime.SpecifyKind(value, DateTimeKind.Utc);
    }
    public static DateTime? SpecifyAsUtc(this DateTime? value) {
      return value != null && value.HasValue ? (DateTime?)DateTime.SpecifyKind(value.Value, DateTimeKind.Utc) : null;
    }
  }
}