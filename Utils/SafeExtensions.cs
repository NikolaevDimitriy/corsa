﻿namespace Corsa.Utils {
  public static class SafeExtensions {
    public static string ToSafeString(this string val) {
      return string.IsNullOrEmpty(val) ? string.Empty : val;
    }

    public static string ToSafeString(this object val) {
      return val == null ? string.Empty : val.ToString();
    }

    public static T Safe<T>(this T val) {
      return val == null ? default(T) : val;
    }
  }
}
