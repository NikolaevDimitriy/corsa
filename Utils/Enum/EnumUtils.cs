using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;

namespace Corsa.Utils.Enum {
  public static class EnumUtils {
    private static readonly ConcurrentDictionary<Type, Lazy<object[]>> _enumCache = new ConcurrentDictionary<Type, Lazy<object[]>>();
    private static readonly ConcurrentDictionary<Type, Lazy<Delegate>> _toIntFuncsCache = new ConcurrentDictionary<Type, Lazy<Delegate>>();
    private static readonly ConcurrentDictionary<Type, Lazy<Delegate>> _fromIntFuncsCache = new ConcurrentDictionary<Type, Lazy<Delegate>>();

    public const int DEFAULT_EXCLUDED_VALUE = -1;
    public const int DEFAULT_FLAGS_VALUE = 0;

    public static IEnumerable<IEnumMember<TEnum>> GetMembers<TEnum>(bool excludeDefault = true) where TEnum : struct {
      return GetMembers<TEnum>(excludeDefault ? new[] { FromInt<TEnum>(DEFAULT_EXCLUDED_VALUE) } : new TEnum[0]);
    }

    public static IEnumerable<IEnumMember<TEnum>> GetMembers<TEnum>(params TEnum[] excluded) where TEnum : struct {
      return GetMembersInternal<TEnum>()
              .Where(m => !excluded.Contains(m.Value))
              .ToArray();
    }

    public static bool IsNameDefined<TEnum>(string name, StringComparison comparisonType = StringComparison.InvariantCultureIgnoreCase) where TEnum : struct {
      return GetMembersInternal<TEnum>()
              .Any(m => m.Name.Equals(name, comparisonType));
    }

    public static bool IsValueDefined<TEnum>(int value) where TEnum : struct {
      return GetMembersInternal<TEnum>()
              .Any(m => m.RawValue == value);
    }

    public static bool IsValueDefined<TEnum>(TEnum value) where TEnum : struct {
      int intValue = ToInt(value);
      return GetMembersInternal<TEnum>()
              .Any(m => m.RawValue == intValue);
    }

    public static IEnumerable<TEnum> GetValues<TEnum>(bool excludeDefault = true) where TEnum : struct {
      return GetValues<TEnum>(excludeDefault ? new[] { FromInt<TEnum>(DEFAULT_EXCLUDED_VALUE) } : new TEnum[0]);
    }

    /// <summary>
    /// Generic get all enum values
    /// </summary>
    public static IEnumerable<TEnum> GetValues<TEnum>(params TEnum[] excluded) where TEnum : struct {
      return GetMembersInternal<TEnum>()
                  .Select(m => m.Value)
                  .Except(excluded)
                  .ToArray();
    }

    public static IDictionary<TEnum, string> GetNamedValues<TEnum>(bool excludeDefault = true) where TEnum : struct {
      return GetNamedValues<TEnum>(excludeDefault ? new[] { FromInt<TEnum>(DEFAULT_EXCLUDED_VALUE) } : new TEnum[0]);
    }

    public static IDictionary<TEnum, string> GetNamedValues<TEnum>(params TEnum[] excluded) where TEnum : struct {
      return GetMembersInternal<TEnum>()
              .Where(m => !excluded.Contains(m.Value))
              .ToDictionary(m => m.Value, m => m.Name);
    }

    public static IDictionary<TEnum, string> GetDescribedValues<TEnum>(bool excludeDefault = true) where TEnum : struct {
      return GetDescribedValues<TEnum>(excludeDefault ? new[] { FromInt<TEnum>(DEFAULT_EXCLUDED_VALUE) } : new TEnum[0]);
    }

    public static IDictionary<TEnum, string> GetDescribedValues<TEnum>(params TEnum[] excluded) where TEnum : struct {
      return GetMembersInternal<TEnum>()
              .Where(m => !excluded.Contains(m.Value))
              .ToDictionary(m => m.Value, m => m.Attributes.Get<DescriptionAttribute>()?.Description ?? m.Name);
    }

    public static IDictionary<TEnum, TAttribute> GetAttributedValues<TEnum, TAttribute>(params TEnum[] excluded)
            where TEnum : struct
            where TAttribute : Attribute {
      return GetMembersInternal<TEnum>()
              .Where(m => !excluded.Contains(m.Value))
              .ToDictionary(m => m.Value, m => m.Attributes.Get<TAttribute>());
    }

    public static IEnumerable<IEnumMember> GetMembers(Type tEnum, bool excludeDefault = true) {
      return GetMembers(excludeDefault ? new[] { DEFAULT_EXCLUDED_VALUE } : new int[0]);
    }

    public static IEnumerable<IEnumMember> GetMembers(Type tEnum, params int[] excluded) {
      return GetMembersInternal(tEnum)
              .Where(m => !excluded.Contains(m.RawValue))
              .ToArray();
    }

    public static bool IsNameDefined(Type tEnum, string name, StringComparison comparisonType = StringComparison.InvariantCultureIgnoreCase) {
      return GetMembersInternal(tEnum)
              .Any(m => m.Name.Equals(name, comparisonType));
    }

    public static bool IsValueDefined(Type tEnum, int value) {
      return GetMembersInternal(tEnum)
              .Any(m => m.RawValue == value);
    }

    public static IEnumerable GetValues(Type tEnum, bool excludeDefault = true) {
      return GetValues(tEnum, excludeDefault ? new[] { DEFAULT_EXCLUDED_VALUE } : new int[0]);
    }

    public static IEnumerable GetValues(Type tEnum, params int[] excluded) {
      return GetMembersInternal(tEnum)
                  .Select(m => m.RawValue)
                  .Except(excluded)
                  .ToArray();
    }

    public static IDictionary<int, string> GetNamedValues(Type tEnum, bool excludeDefault = true) {
      return GetNamedValues(tEnum, excludeDefault ? new[] { DEFAULT_EXCLUDED_VALUE } : new int[0]);
    }

    public static IDictionary<int, string> GetNamedValues(Type tEnum, params int[] excluded) {
      return GetMembersInternal(tEnum)
              .Where(m => !excluded.Contains(m.RawValue))
              .ToDictionary(m => m.RawValue, m => m.Name);
    }

    public static IDictionary<int, string> GetDescribedValues(Type tEnum, bool excludeDefault = true) {
      return GetDescribedValues(tEnum, excludeDefault ? new[] { DEFAULT_EXCLUDED_VALUE } : new int[0]);
    }

    public static IDictionary<int, string> GetDescribedValues(Type tEnum, params int[] excluded) {
      return GetMembersInternal(tEnum)
              .Where(m => !excluded.Contains(m.RawValue))
              .ToDictionary(m => m.RawValue, m => m.Attributes.Get<DescriptionAttribute>()?.Description ?? m.Name);
    }

    public static IDictionary<int, TAttribute> GetAttributedValues<TAttribute>(Type tEnum, params int[] excluded)
            where TAttribute : Attribute {
      return GetMembersInternal(tEnum)
              .Where(m => !excluded.Contains(m.RawValue))
              .ToDictionary(m => m.RawValue, m => m.Attributes.Get<TAttribute>());
    }


    private static IEnumMember[] GetMembersInternal(Type tEnum) =>
        (IEnumMember[])_enumCache.GetOrAdd(tEnum, _ => new Lazy<object[]>(() =>
            tEnum.GetFields(BindingFlags.Public | BindingFlags.Static)
                .Select(f => new EnumMember(f))
                .ToArray()
        , LazyThreadSafetyMode.ExecutionAndPublication)).Value;

    private static IEnumMember<TEnum>[] GetMembersInternal<TEnum>() where TEnum : struct {
      var tEnum = typeof(TEnum);
      return (IEnumMember<TEnum>[])_enumCache.GetOrAdd(tEnum, _ => new Lazy<object[]>(() =>
          tEnum.GetFields(BindingFlags.Public | BindingFlags.Static)
              .Select(f => new EnumMember<TEnum>(f))
              .ToArray()
      , LazyThreadSafetyMode.ExecutionAndPublication)).Value;
    }

    /// <summary>
    /// Метод получения generic-конвертера любого перечисления в int
    /// </summary>
    public static Func<TEnum, int> GetToIntConverter<TEnum>() where TEnum : struct {
      var tEnum = typeof(TEnum);
      return (Func<TEnum, int>)_toIntFuncsCache.GetOrAdd(tEnum, _ => new Lazy<Delegate>(() => {
        var paramT = Expression.Parameter(tEnum);
        UnaryExpression bodyToInt = Expression.Convert(paramT, typeof(int));
        return (Delegate)Expression.Lambda<Func<TEnum, int>>(bodyToInt, paramT).Compile();
      }, LazyThreadSafetyMode.ExecutionAndPublication)).Value;
    }

    /// <summary>
    /// Метод получения generic-конвертера int в любое перечисление
    /// </summary>
    public static Func<int, TEnum> GetFromIntConverter<TEnum>() where TEnum : struct {
      var tEnum = typeof(TEnum);
      return (Func<int, TEnum>)_fromIntFuncsCache.GetOrAdd(tEnum, _ => new Lazy<Delegate>(() => {
        var paramInt = Expression.Parameter(typeof(int));
        UnaryExpression bodyToT = Expression.Convert(paramInt, typeof(TEnum));
        return (Delegate)Expression.Lambda<Func<int, TEnum>>(bodyToT, paramInt).Compile();
      }, LazyThreadSafetyMode.ExecutionAndPublication)).Value;
    }

    public static int ToInt<TEnum>(TEnum value) where TEnum : struct {
      return GetToIntConverter<TEnum>()(value);
    }

    public static TEnum FromInt<TEnum>(int value) where TEnum : struct {
      return GetFromIntConverter<TEnum>()(value);
    }

    public static TEnum FlagsFromEnumerable<TEnum>(IEnumerable<TEnum> values) where TEnum : struct {
      return FromInt<TEnum>(values.Cast<int>().Aggregate(0, (r, x) => r | x));
    }

    public static IEnumerable<TEnum> FlagsToEnumerable<TEnum>(TEnum value) where TEnum : struct {
      int intValue = ToInt(value);
      if (intValue == DEFAULT_FLAGS_VALUE)
        return new TEnum[] { value };

      int maxValue = GetMembersInternal<TEnum>().Max(x => x.RawValue);
      int bit = 1;
      var list = new List<TEnum>();
      while (bit <= maxValue) {
        if ((bit & intValue) > 0)
          list.Add(FromInt<TEnum>(bit));
        bit <<= 1;
      }
      return list.ToArray();
    }

    public static bool FlagsAreDefined<TEnum>(TEnum value) where TEnum : struct {
      return FlagsToEnumerable<TEnum>(value).All(x => IsValueDefined(x));
    }

    public static bool FlagsAreDefined<TEnum>(int value) where TEnum : struct {
      return FlagsToEnumerable<TEnum>(FromInt<TEnum>(value)).Any(x => IsValueDefined(x));
    }

    public interface IEnumMember {
      FieldInfo Field { get; }
      string Name { get; }
      AttributeCollection Attributes { get; }
      int RawValue { get; }
    }

    public interface IEnumMember<TEnum> : IEnumMember where TEnum : struct {
      TEnum Value { get; }
    }

    internal class EnumMember : IEnumMember {
      private readonly AttributeCollection _attributeCollection;
      private readonly int _rawValue;

      internal EnumMember(FieldInfo field) {
        Field = field ?? throw new ArgumentNullException(nameof(field));
        _attributeCollection = new AttributeCollection(Attribute.GetCustomAttributes(Field, false));
        _rawValue = (int)Field.GetValue(null);
      }

      public FieldInfo Field { get; }
      public string Name => Field.Name;

      public AttributeCollection Attributes => _attributeCollection;

      public int RawValue => _rawValue;
    }

    internal class EnumMember<TEnum> : IEnumMember<TEnum> where TEnum : struct {
      private readonly AttributeCollection _attributeCollection;
      private readonly TEnum _value;
      private readonly int _rawValue;

      internal EnumMember(FieldInfo field) {
        Field = field ?? throw new ArgumentNullException(nameof(field));
        _attributeCollection = new AttributeCollection(Attribute.GetCustomAttributes(Field, false));
        _rawValue = (int)Field.GetValue(null);
        _value = EnumUtils.FromInt<TEnum>(_rawValue);
      }

      internal EnumMember(IEnumMember untypedMember) {
        Field = untypedMember.Field;
        _attributeCollection = untypedMember.Attributes;
        _rawValue = untypedMember.RawValue;
        _value = EnumUtils.FromInt<TEnum>(_rawValue);
      }

      public FieldInfo Field { get; }
      public string Name => Field.Name;

      public AttributeCollection Attributes => _attributeCollection;

      public int RawValue => _rawValue;

      public TEnum Value => _value;

      public static implicit operator EnumMember<TEnum>(EnumMember member) {
        return new EnumMember<TEnum>(member);
      }
    }
  }

  public static class EnumMemberExtensions {
    public static EnumUtils.IEnumMember<TEnum> AsT<TEnum>(this EnumUtils.IEnumMember value) where TEnum : struct {
      return new EnumUtils.EnumMember<TEnum>(value);
    }
  }

  public static class AttributeCollectionExtensions {
    public static TAttr Get<TAttr>(this AttributeCollection collection) where TAttr : Attribute {
      return collection.OfType<TAttr>().FirstOrDefault();
    }
  }
}
