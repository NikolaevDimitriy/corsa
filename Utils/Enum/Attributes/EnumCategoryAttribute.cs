using System;
namespace Corsa.Utils.Enum.Attributes {

  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Enum)]
  public abstract class EnumCategoryAttribute : Attribute {
    public Type EnumCategoryType { get; }

    public int EnumCategoryValue { get; }
    public EnumCategoryAttribute(Type enumCategoryType, int value) {
      EnumCategoryType = enumCategoryType;
      EnumCategoryValue = value;
    }
  }
}