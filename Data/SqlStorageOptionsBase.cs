﻿using Corsa.Interfaces.Data;

namespace Corsa.Data {
  public class SqlStorageOptionsBase : IStorageOptions {
    public SqlStorageOptionsBase(string masterConnectionString, string slaveConnectionString) {
      MasterConnectionString = masterConnectionString;
      SlaveConnectionString = slaveConnectionString;
    }
    public string MasterConnectionString { get; private set; }
    public string SlaveConnectionString { get; private set; }
  }
}
