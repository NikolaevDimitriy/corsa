﻿using System;

namespace Corsa.Data.Sql {
  public class SqlStorageImproperlyConfigured : Exception {
    public SqlStorageImproperlyConfigured() : base() { }
    public SqlStorageImproperlyConfigured(string message) : base(message) { }
    public SqlStorageImproperlyConfigured(string message, Exception innerException) : base(message, innerException) { }
  }

  public class SqlStorageUnexpectedParameterType : Exception {
    public SqlStorageUnexpectedParameterType() : base() { }
    public SqlStorageUnexpectedParameterType(string message) : base(message) { }
    public SqlStorageUnexpectedParameterType(string message, Exception innerException) : base(message, innerException) { }

    public SqlStorageUnexpectedParameterType(string expectedType, string givenType) :
        base($"Expected {expectedType} but is got {givenType} parameter type.") {
    }
  }
}