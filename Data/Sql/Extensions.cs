using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using Corsa.Interfaces.Data;
using Corsa.Data.Sql.Parameters;
using System.Globalization;

namespace Corsa.Data.Sql {
  public static class Extensions {
    public static T SetValue<T>(this T param, string name, object value, bool isOutput = false) where T : DbParameter {
      param.ParameterName = string.Format("@{0}", name);
      param.Direction = isOutput == false ? ParameterDirection.Input : ParameterDirection.Output;
      param.Value = value ?? DBNull.Value;
      param.SourceColumn = name;
      return param;
    }

    public static NpgsqlParameter SetPgTypeValue(this NpgsqlParameter param, string name, object value, NpgsqlDbType type, bool isOutput = false) {
      var pgParam = param.SetValue(name, value, isOutput);
      if (type != NpgsqlDbType.Unknown)
        pgParam.NpgsqlDbType = type;
      return pgParam;
    }

    public static NpgsqlDbType GetPgType(this object input) {
      switch (input) {
        case int i: return NpgsqlDbType.Integer;
        case Guid g: return NpgsqlDbType.Uuid;
        default: return NpgsqlDbType.Integer;
      }
    }

    public static Dictionary<T, int> IntersectFieldsToDictionary<T>(this Dictionary<string, T> fieldNames,
        IDictionary<string, int> columnOrdinalDictionary) where T : struct, IConvertible {
      return
          (from column in columnOrdinalDictionary
           from field in fieldNames
           where column.Key.Equals(field.Key, StringComparison.OrdinalIgnoreCase)
           select new { col = field.Value, ordinal = column.Value }).ToDictionary(x => x.col, x => x.ordinal);
    }

    public static Dictionary<string, int> IntersectFieldsToDictionary(this Dictionary<string, string> fieldNames,
        IDictionary<string, int> columnOrdinalDictionary) {
      return
          (from column in columnOrdinalDictionary
           from field in fieldNames
           where column.Key.Equals(field.Key, StringComparison.OrdinalIgnoreCase)
           select new { col = field.Value, ordinal = column.Value }).ToDictionary(x => x.col, x => x.ordinal);
    }

    public static void CheckParameterType(this IParameters<DbParameter> parameters, object target, string methodName) {
#if DEBUG
            var attr = target.GetType().GetTypeInfo().GetCustomAttributes(typeof(SqlParameterCheckAttribute))
                .OfType<SqlParameterCheckAttribute>().FirstOrDefault(el => el.MethodName == methodName);
            if (attr != null && parameters.GetType() != attr.ExpectedType)
                throw new SqlStorageUnexpectedParameterType(attr.ExpectedType.Name, parameters.GetType().Name);
#endif
    }
  }
}