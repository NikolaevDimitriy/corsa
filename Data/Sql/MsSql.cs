﻿using Corsa.Interfaces.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Corsa.Interfaces.Models;
using Microsoft.Extensions.Logging;

namespace Corsa.Data.Sql {
  public abstract class MsSql : SqlBase<SqlDataReader> {
    public MsSql(IStorageOptions options, ILogger logger)
        : base(options, logger) {
    }

    protected override DbConnection GetMasterConnection() {
      if (string.IsNullOrEmpty(Options.MasterConnectionString))
        throw new SqlStorageImproperlyConfigured();
      return new SqlConnection(Options.MasterConnectionString);
    }
    protected override DbConnection GetSlaveConnection() {
      if (string.IsNullOrEmpty(Options.SlaveConnectionString))
        throw new SqlStorageImproperlyConfigured();
      return new SqlConnection(Options.SlaveConnectionString);
    }

    protected override DbParameter CreateParameter() {
      return new SqlParameter();
    }
    protected override DbCommand CreateCommand() {
      return new SqlCommand();
    }
  }
}
