using System;
using System.Collections.Generic;
using System.Data.Common;
using Corsa.Interfaces.Models;
using Corsa.Utils;


namespace Corsa.Data.Sql.Builders {
  public abstract class DeletableModelBuilderBase<TSource, TItem, TId> : ModelBuilderBase<TSource, TItem, TId>
      where TItem : class, IDeletableModel<TId>, new()
      where TSource : DbDataReader {

    private static class DeletableModelFields {
      public const string DELETED = "deleted";
      public const string DELETION_TIMESTAMP = "deletion_timestamp";
      internal static HashSet<string> _fields = new HashSet<string>() {
                DELETED,
                DELETION_TIMESTAMP
            };
    }

    protected DeletableModelBuilderBase(string fieldPrefix) : base(fieldPrefix) {
      InitFieldNames(DeletableModelFields._fields);
    }

    public override TItem Build(TSource obj) {
      TItem item = base.Build(obj);
      item.IsDeleted = ExtractValue<bool>(obj, DeletableModelFields.DELETED);
      item.DeletionTimestamp = ExtractNullableValue<DateTime>(obj, DeletableModelFields.DELETION_TIMESTAMP).SpecifyAsUtc();
      return item;
    }


  }
}