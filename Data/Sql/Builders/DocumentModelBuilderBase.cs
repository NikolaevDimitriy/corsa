using System;
using System.Collections.Generic;
using System.Data.Common;
using Corsa.Models;
using Corsa.Interfaces.Models;
using Corsa.Utils;


namespace Corsa.Data.Sql.Builders {
  public abstract class DocumentModelBuilderBase<TSource, TItem, TId> : DeletableModelBuilderBase<TSource, TItem, TId>
      where TItem : class, IDocumentModel<TId>, new()
      where TSource : DbDataReader {

    private static class DocumentFields {
      public const string CREATOR = "creator";
      public const string CREATOR_NAME = "creator_name";
      public const string CREATION_TIMESTAMP = "creation_timestamp";
      public const string LAST_UPDTE_TIMESTAMP = "last_update_timestamp";

      internal static HashSet<string> _fields = new HashSet<string> {
                CREATOR,
                CREATOR_NAME,
                CREATION_TIMESTAMP,
                LAST_UPDTE_TIMESTAMP
            };
    }

    protected DocumentModelBuilderBase(string fieldPrefix) : base(fieldPrefix) {
      InitFieldNames(DocumentFields._fields);
    }

    public override TItem Build(TSource obj) {
      TItem item = base.Build(obj);
      item.Creator = new IdValueItem<int, string>(ExtractValue<int>(obj, DocumentFields.CREATOR), string.Empty);
      item.CreationTimestamp = ExtractValue<DateTime>(obj, DocumentFields.CREATION_TIMESTAMP).SpecifyAsUtc();
      item.LastUpdateTimestamp = ExtractValue<DateTime>(obj, DocumentFields.LAST_UPDTE_TIMESTAMP).SpecifyAsUtc();
      return item;
    }
  }
}