using System.Collections.Generic;
using System.Data.Common;
using Corsa.Interfaces.Data;
using Corsa.Interfaces.Models;

namespace Corsa.Data.Sql.Builders {

  public abstract class ModelBuilderBase<TSource, TItem, TId> : DbDataReaderBaseExtractor<TSource>, IBuilder<TSource, TItem>
      where TItem : class, IModel<TId>, new()
      where TSource : DbDataReader {
    protected ModelBuilderBase(string fieldPrefix) : base(fieldPrefix) {
      InitFieldNames(ModelFields._fields);
    }

    private static class ModelFields {
      public const string ID = "id";
      internal static HashSet<string> _fields = new HashSet<string>() {
                ID
            };
    }

    public virtual TItem Build(TSource obj) {
      return new TItem
      {
        Id = ExtractValue<TId>(obj, ModelFields.ID)
      };
    }

    protected V ExtractValue<V, TVId>(TSource reader, string field, IModelBuilder<TSource, V> builder)
        where V : class, IModel<TVId>, new() {
      return builder != null
          ? (builder.Build(reader))
          : new V()
          {
            Id = ExtractValue<TVId>(reader, field)
          };
    }
    protected V ExtractStructValue<V, TVId>(TSource reader, string field, IModelBuilder<TSource, V> builder)
        where V : struct, IModel<TVId> {
      return builder != null
          ? (builder.Build(reader))
          : new V()
          {
            Id = ExtractValue<TVId>(reader, field)
          };
    }
  }

}