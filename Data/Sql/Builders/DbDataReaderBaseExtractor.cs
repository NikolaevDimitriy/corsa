using System.Collections.Generic;
using System.Data.Common;
using Newtonsoft.Json;
using NpgsqlTypes;

namespace Corsa.Data.Sql.Builders {
  public abstract class DbDataReaderBaseExtractor<TSource>
      where TSource : DbDataReader {

    private const int ABSENT_INDEX = -1;

    public DbDataReaderBaseExtractor(string fieldPrefix) {
      FieldPrefix = fieldPrefix;
    }

    protected void InitFieldNames(HashSet<string> fields) {
      foreach (var el in fields) {
        FieldNames.Add(FieldPrefix + el, el);
      }
    }

    protected string FieldPrefix { get; set; } = string.Empty;

    protected Dictionary<string, string> FieldNames { get; } = new Dictionary<string, string>();

    protected Dictionary<string, int> IndexedFields { get; set; }

    public virtual void IndexFields(IDictionary<string, int> columnOrdinalDictionary) {
      IndexedFields = FieldNames.IntersectFieldsToDictionary(columnOrdinalDictionary);
    }

    private V ExtractValueByIndex<V>(TSource reader, int index) {
      return index != ABSENT_INDEX && !reader.IsDBNull(index) ? reader.GetFieldValue<V>(index) : default(V);
    }

    private V? ExtractNullableValueByIndex<V>(TSource reader, int index) where V : struct {
      return index != ABSENT_INDEX && !reader.IsDBNull(index) ? (V?)reader.GetValue(index) : null;
    }

    protected V ExtractValue<V>(TSource reader, string field) {
      return IndexedFields.ContainsKey(field) ? ExtractValueByIndex<V>(reader, IndexedFields[field]) : default(V);
    }

    protected V? ExtractNullableValue<V>(TSource reader, string field) where V : struct {
      return IndexedFields.ContainsKey(field) ? ExtractNullableValueByIndex<V>(reader, IndexedFields[field]) : null;
    }

    protected V ExtractValue<V>(TSource reader, string field, NpgsqlDbType type) {
      return IndexedFields.ContainsKey(field)
          ? type == NpgsqlDbType.Jsonb
              ? JsonConvert.DeserializeObject<V>(ExtractValueByIndex<string>(reader, IndexedFields[field]) ?? string.Empty)
              : ExtractValueByIndex<V>(reader, IndexedFields[field])
          : default(V);
    }
  }
}
