using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Npgsql;
using NpgsqlTypes;
using Corsa.Interfaces.Data;
using Corsa.Interfaces.Models;
using Microsoft.Extensions.Logging;

namespace Corsa.Data.Sql {
  public abstract class PgSql : SqlBase<NpgsqlDataReader> {

    protected PgSql(IStorageOptions options, ILogger logger) :
        base(options, logger) {
    }

    protected override DbConnection GetSlaveConnection() {
      return new NpgsqlConnection(Options.SlaveConnectionString);
    }

    protected override DbConnection GetMasterConnection() {
      return new NpgsqlConnection(Options.MasterConnectionString);
    }

    protected override DbCommand CreateCommand() {
      return new NpgsqlCommand();
    }
    protected override DbParameter CreateParameter() {
      return new NpgsqlParameter();
    }

    protected override DbParameter CreateParameter(string name, object value, bool isOutput = false) {
      return new NpgsqlParameter()
      {
        ParameterName = name,
        Direction = isOutput == false ? ParameterDirection.Input : ParameterDirection.Output,
        Value = value ?? DBNull.Value,
        SourceColumn = name
      };
    }
    protected DbParameter CreateParameter(string name, object value, NpgsqlDbType type, bool isOutput = false) {
      return new NpgsqlParameter()
      {
        ParameterName = name,
        Direction = isOutput == false ? ParameterDirection.Input : ParameterDirection.Output,
        Value = value ?? DBNull.Value,
        SourceColumn = name,
        NpgsqlDbType = type
      };
    }
  }
}