using System;
using System.Collections.Generic;
using System.Data.Common;
using Corsa.Interfaces.Data;


namespace Corsa.Data.Sql.Parameters {
  public class IdParameter<T> : IParameters<DbParameter> {
    public IdParameter(T id) {
      Id = id;
    }
    protected T Id { get; set; }
    const string IDENTITY_PARAM_NAME = "identity";
    public IEnumerable<DbParameter> GetParameters(Func<DbParameter> createParameter) {
      yield return createParameter().SetValue(IDENTITY_PARAM_NAME, Id, false);
    }
  }
}