using System;
using System.Collections.Generic;
using System.Data.Common;
using NpgsqlTypes;

namespace Corsa.Data.Sql.Parameters {
  public class SortingPagingParameter<T> : PagingParameter<T> where T : DbParameter {
    public enum SortDirections {
      Asc,
      Desc
    }

    public const string SORT_DIRECTION_ASC = "ASC";
    public const string SORT_DIRECTION_DESC = "DESC";

    protected const string SORT_COLUMN_PARAM_NAME = "sort_col";
    protected const string SORT_DIR_PARAM_NAME = "sort_dir";

    public SortingPagingParameter(int limit, int offset, string sortColumn, SortDirections sortDir) : base(limit, offset) {
      SortColumn = sortColumn;
      SortDir = sortDir;
    }

    public string SortColumn { get; }
    public SortDirections SortDir { get; }

    public override IEnumerable<T> GetParameters(Func<T> createParameter) {
      foreach (var parameter in base.GetParameters(createParameter))
        yield return parameter;
      yield return createParameter().SetValue(SORT_COLUMN_PARAM_NAME, SortColumn);
      yield return createParameter().SetValue(SORT_DIR_PARAM_NAME, SortDir == SortDirections.Asc ? SORT_DIRECTION_ASC : SORT_DIRECTION_DESC);
    }
  }
}