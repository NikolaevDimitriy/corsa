using System;
using System.Collections.Generic;
using System.Data.Common;
using Corsa.Interfaces.Data;

namespace Corsa.Data.Sql.Parameters {
  public class PagingParameter<T> : IPagingParameters<T> where T : DbParameter {

    protected const string LIMIT_PARAM_NAME = "page_limit";
    protected const string OFFSET_PARAM_NAME = "page_offset";

    protected int Limit { get; }
    protected int Offset { get; }

    public PagingParameter(int limit, int offset) {
      Limit = limit;
      Offset = offset;
    }


    public virtual IEnumerable<T> GetParameters(Func<T> createParameter) {
      foreach (var parameter in GetFilterParameters(createParameter))
        yield return parameter;
      yield return createParameter().SetValue(LIMIT_PARAM_NAME, Limit, false);
      yield return createParameter().SetValue(OFFSET_PARAM_NAME, Offset, false);
    }

    public virtual IEnumerable<T> GetFilterParameters(Func<T> createParameter) {
      return new List<T>();
    }
  }
}