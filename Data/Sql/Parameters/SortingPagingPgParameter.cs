using System;
using System.Collections.Generic;
using Npgsql;
using NpgsqlTypes;

namespace Corsa.Data.Sql.Parameters {
  public class SortingPagingPgParameter : SortingPagingParameter<NpgsqlParameter> {

    public SortingPagingPgParameter(int limit, int offset, string sortColumn, SortDirections sortDir) : base(limit, offset, sortColumn, sortDir) { }

    public override IEnumerable<NpgsqlParameter> GetParameters(Func<NpgsqlParameter> createParameter) {
      foreach (var parameter in base.GetParameters(createParameter))
        yield return parameter;
      yield return createParameter().SetPgTypeValue(SORT_COLUMN_PARAM_NAME, SortColumn, NpgsqlDbType.Text);
      yield return createParameter().SetPgTypeValue(SORT_DIR_PARAM_NAME, SortDir == SortDirections.Asc ? SORT_DIRECTION_ASC : SORT_DIRECTION_DESC, NpgsqlDbType.Text);
    }
  }
}