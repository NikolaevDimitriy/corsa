﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using Corsa.Interfaces.Data;


namespace Corsa.Data.Sql.Parameters {
  public class EmptyParameter : IParameters<DbParameter> {

    public IEnumerable<DbParameter> GetParameters(Func<DbParameter> createParameter) {
      return new List<DbParameter>();
      //Не работает, т.к. возвращается пустой параметр
      //if(createParameter != null)
      //yield return createParameter();
    }
  }
}
