using System;
using System.Collections.Generic;
using Npgsql;
using NpgsqlTypes;

namespace Corsa.Data.Sql.Parameters {
  public class PagingPgParameter : PagingParameter<NpgsqlParameter> {

    public PagingPgParameter(int limit, int offset) : base(limit, offset) { }

    public override IEnumerable<NpgsqlParameter> GetParameters(Func<NpgsqlParameter> createParameter) {
      foreach (var parameter in GetFilterParameters(createParameter))
        yield return parameter;
      yield return createParameter().SetPgTypeValue(LIMIT_PARAM_NAME, Limit, NpgsqlDbType.Integer);
      yield return createParameter().SetPgTypeValue(OFFSET_PARAM_NAME, Offset, NpgsqlDbType.Integer);
    }
  }
}