﻿using System;

namespace Corsa.Data.Sql.Parameters {
  [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
  public class SqlParameterCheckAttribute : Attribute {
    public SqlParameterCheckAttribute(Type expectedType, string methodName) {
      ExpectedType = expectedType;
      MethodName = methodName;
    }
    public string MethodName { get; }
    public Type ExpectedType { get; }
  }
}