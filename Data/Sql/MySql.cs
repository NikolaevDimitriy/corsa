﻿using System.Data.Common;
using Corsa.Interfaces.Data;
using Microsoft.Extensions.Logging;
using Corsa.Interfaces.Models;
using MySql.Data.MySqlClient;

namespace Corsa.Data.Sql {
  public abstract class MySql : SqlBase<MySqlDataReader> {
    
    protected MySql(IStorageOptions options, ILogger logger)
        : base(options, logger) {
    }

    protected override DbConnection GetSlaveConnection() {
      return new MySqlConnection(Options.SlaveConnectionString);
    }

    protected override DbConnection GetMasterConnection() {
      return new MySqlConnection(Options.MasterConnectionString);
    }

    protected override DbParameter CreateParameter() {
      return new MySqlParameter();
    }

    protected override DbCommand CreateCommand() {
      return new MySqlCommand();
    }
  }
}
