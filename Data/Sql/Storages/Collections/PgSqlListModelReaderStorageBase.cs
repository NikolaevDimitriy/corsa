using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Npgsql;
using Corsa.Interfaces.Data;
using Corsa.Interfaces.Models;

namespace Corsa.Data.Sql.Storages.Collections {
  public abstract class PgSqlListModelReaderStorageBase<TItem, TParam> : PgSqlStorageBase<TItem>,
      IListModelReaderStorage<TItem, TParam, DbParameter>
      where TParam : IParameters<DbParameter> {
    protected PgSqlListModelReaderStorageBase(
        IStorageOptions options,
        ILogger logger,
        IModelBuilder<NpgsqlDataReader, TItem> builder) : base(options, logger, builder) { }

    //Initialize in constructor
    protected string CommandGet;
    protected CommandType CommandGetType = CommandType.Text;

    public virtual async Task<IEnumerable<TItem>> GetList(TParam parameters) {
      parameters.CheckParameterType(this, "GetList");
      if (string.IsNullOrEmpty(CommandGet))
        throw new SqlStorageImproperlyConfigured();
      DbCommand command = BuildCommand(CommandGet, CommandGetType, parameters.GetParameters(CreateParameter).ToArray());
      try {
        return await ExecuteReader(command, ConvertAsync);
      } catch (Exception x) {
        Logger.LogError(x, string.Empty);
        return new List<TItem>();
      }
    }
  }
}