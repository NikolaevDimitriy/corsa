using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Npgsql;
using Corsa.Interfaces.Data;
using Corsa.Interfaces.Models;
using Corsa.Models;

namespace Corsa.Data.Sql.Storages.Collections {
  public abstract class PgSqlModelPaginatorStorageBase<TItem, TSourceParam>
      : PgSqlListModelReaderStorageBase<TItem, TSourceParam>,
        IModelPaginatorStorage<TItem, TSourceParam, DbParameter>
      where TSourceParam : IPagingParameters<DbParameter> {

    protected PgSqlModelPaginatorStorageBase(
        IStorageOptions options, ILogger logger, IModelBuilder<NpgsqlDataReader, TItem> builder)
        : base(options, logger, builder) { }

    //Initialize in constructor
    protected string CommandTotalCountGet;
    protected CommandType CommandTotalCountGetType = CommandType.Text;

    public virtual async Task<long> GetTotalCount(TSourceParam parameters) {
      parameters.CheckParameterType(this, "GetTotalCount");
      if (string.IsNullOrEmpty(CommandTotalCountGet))
        throw new SqlStorageImproperlyConfigured();

      DbCommand command = BuildCommand(CommandTotalCountGet, CommandTotalCountGetType, parameters.GetFilterParameters(CreateParameter).ToArray());
      try {
        return await ExecuteScalar<long>(command);
      } catch (Exception) {
        return 0;
      }
    }

    public async Task<IPagingList<TItem>> GetPage(TSourceParam parameters) {
      parameters.CheckParameterType(this, "GetPage");

      return await Task.Factory.ContinueWhenAll(new Task[] { GetTotalCount(parameters), GetList(parameters) },
          completed => new PagingList<TItem>(
              (completed[1] as Task<IEnumerable<TItem>>).Result,
              (completed[0] as Task<long>).Result));
    }
  }
}