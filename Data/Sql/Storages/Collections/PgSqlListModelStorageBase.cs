using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Linq;
using Microsoft.Extensions.Logging;
using Npgsql;
using Corsa.Interfaces.Data;
using Corsa.Interfaces.Models;

namespace Corsa.Data.Sql.Storages.Collections {
  public abstract class PgSqlListModelStorageBase<TItem, TItemId, TSourceParam> :
      PgSqlListModelReaderStorageBase<TItem, TSourceParam>, IListModelStorage<TItem, TSourceParam, DbParameter>
      where TItem : IModel<TItemId>
      where TSourceParam : class, IParameters<DbParameter> {
    protected PgSqlListModelStorageBase(
        IStorageOptions options,
        ILogger logger,
        IModelBuilder<NpgsqlDataReader, TItem> builder)
        : base(options, logger, builder) {
    }

    protected const string PARAM_ID_LIST_NAME = "identities";

    //Initialize in constructor
    protected string CommandSave, CommandDel;
    protected CommandType CommandSaveType = CommandType.Text, CommandDelType = CommandType.Text;

    public async Task<bool> DeleteList(IEnumerable<TItem> items, TSourceParam parameters = null) {
      if (string.IsNullOrEmpty(CommandDel))
        throw new SqlStorageImproperlyConfigured();
      IEnumerable<TItemId> arrIdForDelete = items.Where(el => !el.IsNew).Select(el => el.Id);

      List<DbParameter> listParameters = parameters != null ? parameters.GetParameters(CreateParameter).ToList() : new List<DbParameter>();
      listParameters.Add(CreateParameter(PARAM_ID_LIST_NAME, arrIdForDelete,
          NpgsqlTypes.NpgsqlDbType.Array | arrIdForDelete.FirstOrDefault().GetPgType()));
      DbCommand command = BuildCommand(CommandDel, CommandDelType, listParameters.ToArray());
      try {
        await ExecuteVoid(command);
        return true;
      } catch {
        return false;
      }
    }

    public Task<IEnumerable<TItem>> SaveList(IEnumerable<TItem> items, TSourceParam parameters = null) {
      throw new NotImplementedException();
    }
  }
}