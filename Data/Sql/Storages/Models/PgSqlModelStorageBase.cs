using System;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Npgsql;
using Corsa.Interfaces.Models;
using Corsa.Interfaces.Data;


namespace Corsa.Data.Sql.Storages.Models {
  public abstract class PgSqlModelStorageBase<TItem, TItemId, TSourceParam>
      : PgSqlModelReaderStorageBase<TItem, TSourceParam>, IModelStorage<TItem, TSourceParam, DbParameter>
      where TItem : IModel<TItemId>
      where TSourceParam : class, IParameters<DbParameter> {
    protected PgSqlModelStorageBase(
        IStorageOptions options, ILogger logger,
        IModelBuilder<NpgsqlDataReader, TItem> builder)
        : base(options, logger, builder) {
    }
    protected const string PARAM_ID_NAME = "identity";

    //Initialize in constructor
    protected string CommandSave, CommandDel;
    protected CommandType CommandSaveType = CommandType.Text, CommandDelType = CommandType.Text;

    public virtual async Task<bool> Delete(TItem item, TSourceParam p = null) {
      if (string.IsNullOrEmpty(CommandDel))
        throw new SqlStorageImproperlyConfigured();
      if (item.IsNew)
        return false;
      DbCommand command = BuildCommand(CommandDel, CommandDelType, CreateParameter(PARAM_ID_NAME, item.Id, false));
      try {
        await ExecuteVoid(command);
        return true;
      } catch {
        return false;
      }
    }

    public virtual async Task<TItem> Save(TItem item, TSourceParam p = null) {
      if (string.IsNullOrEmpty(CommandSave))
        throw new SqlStorageImproperlyConfigured();

      DbCommand command = BuildCommand(CommandSave, CommandSaveType, CreateParamsForSaving(item, p).ToArray());
      try {
        var res = await ExecuteScalar<TItemId>(command);
        if (item.IsNew)
          item.Id = res;
        return item;
      } catch (Exception) {
        return default(TItem);
      }
    }
    protected virtual IEnumerable<DbParameter> CreateParamsForSaving(TItem item, TSourceParam p = null) {
      yield return CreateParameter(PARAM_ID_NAME, item.Id, false);
    }
  }
}
