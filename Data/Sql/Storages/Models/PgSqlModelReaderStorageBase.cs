using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Logging;
using Npgsql;
using Corsa.Interfaces.Data;

namespace Corsa.Data.Sql.Storages.Models {
  public abstract class PgSqlModelReaderStorageBase<TItem, TSourceParam>
      : PgSqlStorageBase<TItem>, IModelReaderStorage<TItem, TSourceParam, DbParameter>
      where TSourceParam : class, IParameters<DbParameter> {
    protected PgSqlModelReaderStorageBase(
        IStorageOptions options, ILogger logger, IModelBuilder<NpgsqlDataReader, TItem> builder)
        : base(options, logger, builder) { }

    //Initialize in constructor
    protected string CommandGet;
    protected CommandType CommandGetType = CommandType.Text;

    public virtual async Task<TItem> Get(TSourceParam param) {
      if (string.IsNullOrEmpty(CommandGet) || Builder == null || param == null)
        throw new SqlStorageImproperlyConfigured();

      DbCommand command = BuildCommand(CommandGet, CommandGetType, param.GetParameters(CreateParameter).ToArray());
      try {
        var items = await ExecuteReader(command, ConvertAsync);
        return items.FirstOrDefault();
      } catch {
        return default(TItem);
      }
    }
  }
}