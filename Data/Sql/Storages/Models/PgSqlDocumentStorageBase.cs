using System;
using System.Collections.Generic;
using System.Data.Common;
using Microsoft.Extensions.Logging;
using Npgsql;
using Corsa.Interfaces.Models;
using Corsa.Interfaces.Data;


namespace Corsa.Data.Sql.Storages.Models {
  public abstract class PgSqlDocumentStorageBase<TItem, TItemId, TParam> : PgSqlModelStorageBase<TItem, TItemId, TParam>
      where TItem : IDocumentModel<TItemId>
      where TParam : class, IParameters<DbParameter> {
    protected PgSqlDocumentStorageBase(
        IStorageOptions options,
        ILogger logger,
        IModelBuilder<NpgsqlDataReader, TItem> builder)
        : base(options, logger, builder) {

    }
    protected override IEnumerable<DbParameter> CreateParamsForSaving(TItem item, TParam p = null) {
      foreach (var prmtr in base.CreateParamsForSaving(item, p))
        yield return prmtr;

      yield return CreateParameter("creation_timestamp", item.CreationTimestamp, NpgsqlTypes.NpgsqlDbType.Timestamp);
      yield return CreateParameter("last_update_timestamp", DateTime.UtcNow, NpgsqlTypes.NpgsqlDbType.Timestamp);
      yield return CreateParameter("creator", item.Creator?.Id, NpgsqlTypes.NpgsqlDbType.Integer);
    }
  }
}
