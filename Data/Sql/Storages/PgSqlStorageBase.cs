using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Logging;
using Npgsql;
using Corsa.Interfaces.Data;
using Corsa.Interfaces.Models;

namespace Corsa.Data.Sql.Storages {
  public abstract class PgSqlStorageBase<TItem> : PgSql {
    protected PgSqlStorageBase(
        IStorageOptions options,
        ILogger logger,
        IModelBuilder<NpgsqlDataReader, TItem> builder)
        : base(options, logger) {
      Builder = builder;
    }

    protected IModelBuilder<NpgsqlDataReader, TItem> Builder { get; private set; }

    protected async Task<IEnumerable<TItem>> ConvertAsync(NpgsqlDataReader reader) {
      var result = new List<TItem>();
      if (reader.HasRows) {
        Builder.IndexFields(GetColumnOrdinalDictionary(reader));
        while (await reader.ReadAsync()) {
          result.Add(Builder.Build(reader));
        }
      }
      return result;
    }
  }
}