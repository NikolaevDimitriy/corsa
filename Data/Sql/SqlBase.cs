using Corsa.Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Corsa.Interfaces.Models;
using Microsoft.Extensions.Logging;

namespace Corsa.Data.Sql {
  public abstract class SqlBase<TSource> where TSource : DbDataReader {
    protected readonly IStorageOptions Options;
    protected readonly ILogger Logger;

    protected SqlBase(IStorageOptions options, ILogger logger) {
      Options = options;
      Logger = logger;
    }

    /// <summary>
    /// must return particular implamentation of DbParameter inheritor
    /// </summary>
    /// <returns></returns>
    protected abstract DbParameter CreateParameter(string name, object value, bool isOutput = false);
    protected abstract DbParameter CreateParameter();

    /// <summary>
    /// must return particular implamentation of DbCommand inheritor
    /// </summary>
    /// <returns></returns>
    protected abstract DbCommand CreateCommand();

    /// <summary>
    /// must return particular implamentation of DbConnection inheritor
    /// </summary>
    /// <returns></returns>
    protected abstract DbConnection GetSlaveConnection();

    /// <summary>
    /// must return particular implamentation of DbConnection inheritor
    /// </summary>
    /// <returns></returns>
    protected abstract DbConnection GetMasterConnection();

    protected async Task<T> ExecuteReader<T>(DbCommand command, Func<TSource, Task<T>> converter, bool useMaster = false) {
      using (var conn = useMaster ? GetMasterConnection() : GetSlaveConnection()) {
        await conn.OpenAsync();
        command.Connection = conn;
        using (var reader = await command.ExecuteReaderAsync()) {
          return await converter(reader as TSource);
        }
      }
    }

    protected async Task<T> ExecuteScalar<T>(DbCommand command, bool useMaster = true) {
      using (var conn = useMaster ? GetMasterConnection() : GetSlaveConnection()) {
        await conn.OpenAsync();
        command.Connection = conn;
        var res = await command.ExecuteScalarAsync();
        return res is T ? (T)res : default(T);
      }
    }

    protected async Task ExecuteVoid(DbCommand command) {
      using (var conn = GetMasterConnection()) {
        await conn.OpenAsync();
        command.Connection = conn;
        await command.ExecuteScalarAsync();
      }
    }

    protected DbCommand BuildCommand(string commandText, CommandType commandType, params DbParameter[] parameters) {
      var command = CreateCommand();
      command.CommandText = commandText;
      command.CommandType = commandType;
      command.CommandTimeout = 100;
      command.Parameters.AddRange(parameters);
      return command;
    }

    protected IDictionary<string, int> GetColumnOrdinalDictionary(TSource reader) {
      return Enumerable.Range(0, reader.FieldCount).ToDictionary(reader.GetName, el => el);
    }
  }
}