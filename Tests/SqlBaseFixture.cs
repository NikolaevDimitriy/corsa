using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Corsa.Interfaces.Models;
using Corsa.Interfaces.Data;
using Corsa.Data;

namespace Corsa.Tests {
  public abstract class SqlBaseFixture : LaunchSettingsFixture {
    protected readonly Random Random;

    protected IConfigurationRoot Config { get; set; }
    protected IRequester<int> Requester { get; set; }

    protected IStorageOptions Options { get; set; }

    protected string MasterConnectionString { get => Config.GetConnectionString("PgMaster"); }
    protected string SlaveConnectionString { get => Config.GetConnectionString("PgSlave"); }



    public SqlBaseFixture(Type testsType, IRequester<int> requester) : base() {
      Requester = requester;
      var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
      Config = new ConfigurationBuilder()
      .SetBasePath(Path.GetDirectoryName(testsType.GetTypeInfo().Assembly.Location))
      .AddJsonFile("testsettings.json", optional: true)
      .AddJsonFile($"testsettings.{env}.json", optional: true)
      .Build();

      Options = new SqlStorageOptionsBase(MasterConnectionString, SlaveConnectionString);
      Random = new Random();
    }

    protected string RandomString(int length) {
      const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      return new string(Enumerable.Repeat(chars, length)
        .Select(s => s[Random.Next(s.Length)]).ToArray());
    }
  }
}
