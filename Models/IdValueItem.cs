using System.Collections.Generic;
using Corsa.Interfaces.Models;

namespace Corsa.Models {
  public class IdValueItem<TId, TVal> : IIdValue<TId, TVal> {
    public TVal Value { get; private set; }
    public TId Id { get; set; }
    public virtual bool IsNew => default(TId).Equals(Id);

    public IdValueItem() {
      Id = default(TId);
      Value = default(TVal);
    }

    public IdValueItem(TId id, TVal value) {
      Id = id;
      Value = value;
    }

    public IdValueItem(KeyValuePair<TId, TVal> keyVal) {
      Id = keyVal.Key;
      Value = keyVal.Value;
    }
  }
}
