using System;

namespace Corsa.Models.Result {
  public readonly struct Result<T> {
    public Result(bool isSuccess, T result, Error error) {
      IsSuccess = isSuccess;
      Error = error;
      Value = result;
    }
    public readonly bool IsSuccess { get; }
    public readonly Error Error { get; }
    public readonly T Value { get; }
  }

  public readonly struct Result {
    private static readonly Result<string> OkResult = new Result<string>(true, "Ok", null);
    public static Result<string> Ok() {
      return OkResult;
    }
    public static Result<T> Create<T>(bool isSuccess, T result, Error error) {
      if (isSuccess) {
        if (error != null)
          throw new ArgumentException("", nameof(error));
      } else {
        if (error == null)
          throw new ArgumentNullException(nameof(error), "");
      }
      return new Result<T>(isSuccess, result, error);
    }
  }
}