namespace Corsa.Models.Result {
  public class Error {
    public Error(string msg, int code, string path = null, object data = null) {
      Msg = msg;
      Code = code;
      Path = path;
      Data = data;
    }
    public readonly string Msg;
    public readonly int Code;
    public readonly string Path;
    public readonly object Data;
  }
}