﻿namespace Corsa.Models {
  public struct Dimensions {
    public enum Measurements {
      mm,
      cm,
      dm,
      m
    }

    public Measurements Measurement;

    public double Length;
    public double Width;
    public double Height;

    public double Volume {
      get { return Length * Width * Height; }
    }
  }
}
