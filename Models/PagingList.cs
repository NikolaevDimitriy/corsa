using System.Collections.Generic;
using Corsa.Interfaces.Models;

namespace Corsa.Models {
  public class PagingList<T> : List<T>, IPagingList<T> {
    public PagingList(long total) : base() {
      Total = total;
    }

    public PagingList(IEnumerable<T> items, long total) : base(items) {
      Total = total;
    }
    public long Total { get; }
  }
}