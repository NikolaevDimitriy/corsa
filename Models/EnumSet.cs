using System;
using System.Collections.Generic;
using System.Linq;
using Corsa.Interfaces.Models;
using Corsa.Utils.Enum;


namespace Corsa.Models {
  /// <summary>
  /// Набор значений перечисления
  /// </summary>
  public class EnumSet<T> : IEnumCompressed<T> where T : struct, Enum {
    /// <summary>
    /// Позиция в массиве данных
    /// </summary>
    private struct EnumSetPosition : IEquatable<EnumSetPosition> {
      /// <summary>
      /// Индекс в массиве
      /// </summary>
      public int ChunkIndex { get; }
      /// <summary>
      /// Позиция бита (смещение)
      /// </summary>
      public int BitShift { get; }

      public int AbsIndex => ChunkIndex * BIT_COUNT + BitShift + 1;

      public EnumSetPosition(int index, int shift) {
        if (index >= MAX_CHUNKS || index < 0)
          throw new ArgumentOutOfRangeException(nameof(index));
        if (shift >= BIT_COUNT || shift < 0)
          throw new ArgumentOutOfRangeException(nameof(shift));
        ChunkIndex = index;
        BitShift = shift;
      }

      public EnumSetPosition(int absIndex) {
        if (absIndex > MAX_ENUM_INDEX || absIndex < 0)
          throw new ArgumentOutOfRangeException(nameof(absIndex));
        int idx = Math.DivRem(absIndex, BIT_COUNT, out int shift);
        if (shift == 0 && idx == 0) {
          BitShift = 0;
          ChunkIndex = 0;
          return;
        }

        // считаем, что биты пронумерованы от 1 до 32, но смещать бит проверки мы должны соответственно от 0 до 31
        shift--;
        if (shift < 0 && idx > 0) {
          // для 32-го индекса надо возвращаться в предыдущий chunk
          idx--;
          shift = BIT_COUNT - 1;
        }

        BitShift = shift;
        ChunkIndex = idx;
      }

      public EnumSetPosition(T enumValue) : this(_asInt(enumValue)) {
      }

      public override bool Equals(object obj) {
        if (ReferenceEquals(obj, null))
          return false;
        return Equals((EnumSetPosition)obj);
      }

      public bool Equals(EnumSetPosition other) {
        return ChunkIndex == other.ChunkIndex && BitShift == other.BitShift;
      }

      public override int GetHashCode() {
        return ChunkIndex * 32 + BitShift;
      }

      /// <summary>
      /// Установленный бит для проверки в chunk
      /// </summary>
      public int CheckBit => (1 << BitShift);

      public EnumSetPosition Next() {
        return new EnumSetPosition(AbsIndex + 1);
      }

      public EnumSetPosition Prev() {
        return new EnumSetPosition(AbsIndex - 1);
      }

      /// <summary>
      /// Позиция по умолчанию
      /// </summary>
      public static readonly EnumSetPosition Default = new EnumSetPosition(0, 0);
    }

    private readonly int[] _rawData;
    private const int BIT_COUNT = 32;

    /// <summary>
    /// Максимальное значение (индекс бита) в перечислении
    /// </summary>
    private static readonly int MAX_ENUM_INDEX;

    /// <summary>
    /// Максимальное число элементов массива данных
    /// </summary>
    private static readonly int MAX_CHUNKS;

    private static readonly Func<T, int> _asInt;
    private static readonly Func<int, T> _asT;

    static EnumSet() {
      MAX_ENUM_INDEX = GetMaxEnumIndex();
      MAX_CHUNKS = Math.DivRem(MAX_ENUM_INDEX, BIT_COUNT, out int shift) + (shift > 0 ? 1 : 0);

      _asInt = EnumUtils.GetToIntConverter<T>();
      _asT = EnumUtils.GetFromIntConverter<T>();
    }

    /// <summary>
    /// Получение максимального значения из перечисления
    /// </summary>
    /// <returns></returns>
    private static int GetMaxEnumIndex() {
      var values = Enum.GetValues(typeof(T));
      if (values.Length > 0) {
        Array.Sort(values);
        return (int)values.GetValue(values.Length - 1);
      }

      return 0;
    }

    /// <summary>
    /// Получить массив данных правильного размера
    /// </summary>
    private static int[] GetChunkArray() => new int[MAX_CHUNKS];

    /// <summary>
    /// Создание набора значений из "сырых" данных
    /// </summary>
    /// <param name="rawData">"Сырые" данные разрешений</param>
    public EnumSet(int[] rawData) {
      if (rawData == null || rawData.Length == 0)
        _rawData = GetChunkArray();
      else
        _rawData = rawData.ToArray();
    }

    /// <summary>
    /// Создание набора значений, путем копирования данных из другого
    /// </summary>
    /// <param name="otherSet"></param>
    public EnumSet(IEnumCompressed<T> otherSet) {
      _rawData = otherSet.ToArray();
    }

    /// <summary>
    /// Создание набора из массива значений перечисления
    /// </summary>
    /// <param name="enumValues">Массив значений перечисления</param>
    public EnumSet(params T[] enumValues) {
      _rawData = GetChunkArray();
      if (enumValues == null)
        return;
      for (int i = 0; i < enumValues.Length; i++) {
        var pos = new EnumSetPosition(enumValues[i]);
        _rawData[pos.ChunkIndex] |= pos.CheckBit;
      }
    }

    /// <summary>
    /// Получить набор со всеми установленными значениями перечисления
    /// </summary>
    public static EnumSet<T> GetFullSet() {
      var values = (T[])Enum.GetValues(typeof(T));
      return new EnumSet<T>(values);
    }

    public int ChunkCount => _rawData.Length;

    public bool IsEmpty => !_rawData.Any(el => el > 0);

    /// <summary>
    /// Объединение наборов значений перечисления
    /// </summary>
    /// <param name="otherSet">Второй набор значений перечисления</param>
    /// <returns>Новый объединённый набор значений перечисления</returns>
    public IEnumCompressed<T> Union(IEnumCompressed<T> otherSet) {
      if (otherSet is EnumSet<T> enumSet) {
        if (ChunkCount != enumSet.ChunkCount)
          throw new ArgumentException("Enum sets are not of same length", nameof(otherSet));
        int[] newSet = GetChunkArray();
        for (int i = 0; i < ChunkCount; i++) {
          newSet[i] = _rawData[i] | enumSet._rawData[i];
        }

        return new EnumSet<T>(newSet);
      }

      throw new NotSupportedException();
    }

    /// <summary>
    /// Инвертировать текущий набор
    /// </summary>
    public void Invert() {
      for (int i = 0; i < ChunkCount; i++) {
        _rawData[i] = ~_rawData[i];
      }
    }

    /// <summary>
    /// Пересечение наборов значений перечисления
    /// </summary>
    /// <param name="otherSet">Второй набор значений перечисления</param>
    /// <returns>Новый набор общих значений перечисления</returns>
    public IEnumCompressed<T> Intersect(IEnumCompressed<T> otherSet) {
      if (otherSet is EnumSet<T> enumSet) {
        if (ChunkCount != enumSet.ChunkCount)
          throw new ArgumentException("Enum sets are not of same length", nameof(otherSet));
        int[] newSet = GetChunkArray();
        for (int i = 0; i < ChunkCount; i++) {
          newSet[i] = _rawData[i] & enumSet._rawData[i];
        }

        return new EnumSet<T>(newSet);
      }

      throw new NotSupportedException();
    }

    /// <summary>
    /// Проверка установки всех требуемых значений в текущем наборе
    /// </summary>
    /// <param name="instance">Набор значений для проверки</param>
    /// <returns>Истина, если все требуемые значения установлены</returns>
    public bool All(IEnumCompressed<T> instance) {
      if (instance is EnumSet<T> enumSet) {
        return All(enumSet);
      }

      return false;
    }

    /// <summary>
    /// Проверка установки всех требуемых значений в текущем наборе
    /// </summary>
    /// <param name="instance">Набор значений для проверки</param>
    /// <returns>Истина, если все требуемые значения установлены</returns>
    public bool All(EnumSet<T> instance) {
      if (ChunkCount != instance.ChunkCount)
        throw new ArgumentException("Enum sets are not of same length", nameof(instance));
      for (var i = 0; i < ChunkCount; i++) {
        if ((_rawData[i] & instance._rawData[i]) != instance._rawData[i])
          return false;
      }

      return true;
    }

    /// <summary>
    /// Проверка установки всех требуемых значений в текущем наборе
    /// </summary>
    /// <param name="items">Массив значений для проверки</param>
    /// <returns>Истина, если все требуемые значения установлены</returns>
    public bool All(params T[] items) {
      if (items == null)
        throw new ArgumentNullException(nameof(items));
      return All(new EnumSet<T>(items));
    }

    /// <summary>
    /// Проверка уставноки хотя бы одного значения в текущем наборе
    /// </summary>
    /// <param name="instance">Набор значений для проверки</param>
    /// <returns>Истина, если все хотя бы одно значение установлено</returns>
    public bool Any(IEnumCompressed<T> instance) {
      if (instance is EnumSet<T> enumSet) {
        return Any(enumSet);
      }

      return false;
    }

    /// <summary>
    /// Проверка уставноки хотя бы одного значения в текущем наборе
    /// </summary>
    /// <param name="instance">Набор значений для проверки</param>
    /// <returns>Истина, если все хотя бы одно значение установлено</returns>
    public bool Any(EnumSet<T> instance) {
      if (ChunkCount != instance.ChunkCount)
        throw new ArgumentException("Enum sets are not of same length", nameof(instance));
      for (var i = 0; i < ChunkCount; i++) {
        if ((_rawData[i] & instance._rawData[i]) == instance._rawData[i])
          return true;
      }

      return false;
    }

    /// <summary>
    /// Проверка уставноки хотя бы одного значения в текущем наборе
    /// </summary>
    /// <param name="items">Массив значений для проверки</param>
    /// <returns>Истина, если все хотя бы одно значение установлено</returns>
    public bool Any(params T[] items) {
      if (items == null)
        throw new ArgumentNullException(nameof(items));

      return items.Any(Contains);
    }

    /// <summary>
    /// Проверка, что в массиве установлено требуемое значение
    /// </summary>
    /// <param name="item">Требуемое значение</param>
    /// <returns>Истина, если значение установлено</returns>
    public bool Contains(T item) {
      var pos = new EnumSetPosition(item);
      return (_rawData[pos.ChunkIndex] & pos.CheckBit) != 0;
    }

    /// <summary>
    /// Получить копию внутреннего массива данных
    /// </summary>
    public int[] ToArray() => _rawData.ToArray();

    /// <summary>
    /// Получить установленные значения
    /// </summary>
    public T[] GetValues() {
      var items = new List<T>();
      for (int i = 0; i < MAX_CHUNKS; i++) {
        if (_rawData[i] == 0)
          continue;
        for (int j = 0; j < BIT_COUNT; j++) {
          var pos = new EnumSetPosition(i, j);
          if ((uint)pos.CheckBit > (uint)_rawData[i])
            continue;
          if ((_rawData[i] & pos.CheckBit) == pos.CheckBit)
            items.Add(_asT(pos.AbsIndex));
        }
      }

      return items.ToArray();
    }

    /// <summary>
    /// Установить флаг (значение)
    /// </summary>
    public virtual void Set(T item) {
      var pos = new EnumSetPosition(item);
      _rawData[pos.ChunkIndex] |= pos.CheckBit;
    }

    public virtual void Set(params T[] items) {
      if (items == null)
        throw new ArgumentNullException(nameof(items));
      if (items.Length == 0)
        return;
      var newSet = new EnumSet<T>(items);
      for (int i = 0; i < ChunkCount; i++) {
        _rawData[i] |= newSet._rawData[i];
      }
    }

    /// <summary>
    /// Снять флаг (значение)
    /// </summary>
    public virtual void Unset(T item) {
      var pos = new EnumSetPosition(item);
      _rawData[pos.ChunkIndex] &= ~pos.CheckBit;
    }

    public virtual void Unset(params T[] items) {
      if (items == null)
        throw new ArgumentNullException(nameof(items));
      if (items.Length == 0)
        return;
      var newSet = new EnumSet<T>(items);
      for (int i = 0; i < ChunkCount; i++) {
        _rawData[i] &= ~newSet._rawData[i];
      }
    }

    public static explicit operator int[](EnumSet<T> enumSet) {
      return enumSet.ToArray();
    }

    public static explicit operator EnumSet<T>(int[] rawData) {
      return new EnumSet<T>(rawData);
    }
  }

  /// <summary>
  /// Набор значений перечисления (только для чтения)
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class ReadOnlyEnumSet<T> : EnumSet<T> where T : struct, Enum {
    public ReadOnlyEnumSet(int[] rawData) : base(rawData) {
    }

    public ReadOnlyEnumSet(params T[] enumValues) : base(enumValues) {
    }

    public ReadOnlyEnumSet(EnumSet<T> set) : base(set.ToArray()) {
    }

    public override void Set(T item) {
      throw new NotSupportedException("The set is readonly and cannot be modified");
    }

    public override void Unset(T item) {
      throw new NotSupportedException("The set is readonly and cannot be modified");
    }
  }
}