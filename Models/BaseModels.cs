using System;
using Corsa.Interfaces.Models;

namespace Corsa.Models {
  public abstract class Model<T> : IModel<T> {
    public T Id { get; set; }
    public virtual bool IsNew => Id.Equals(default(T));
  }

  public abstract class DeletableModel<T> : Model<T>, IDeletableModel<T> {
    public bool IsDeleted { get; set; }
    public DateTime? DeletionTimestamp { get; set; }

    public void Delete() {
      IsDeleted = true;
      DeletionTimestamp = DateTime.UtcNow;
    }
  }
  public abstract class DocumentModel<T> : DeletableModel<T>, IDocumentModel<T> {
    public virtual IIdValue<int, string> Creator { get; set; }
    public DateTime CreationTimestamp { get; set; }
    public DateTime LastUpdateTimestamp { get; set; }

    public DocumentModel(IIdValue<int, string> creator, DateTime creationTimeStamp) {
      Creator = creator;
      CreationTimestamp = creationTimeStamp == DateTime.MinValue ? DateTime.UtcNow : creationTimeStamp;
      LastUpdateTimestamp = creationTimeStamp == DateTime.MinValue ? DateTime.UtcNow : creationTimeStamp;
    }
  }
}